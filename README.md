# Basic Language processing Api
Take the message as input with isAuthenticated

# Config
<pre>
webservices:
  intentDispatcher: "http://intent-dispatcher:9001"

intents:
  GREETING:
    - salut
    - bonjour
    - hello
    - yop
    - hi
  TIME:
    - "!heure"
    - "!hour"
    - "!time"
</pre>