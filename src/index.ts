import express, { Response, Request } from "express";
const config = require("config-yml");
import axios, { AxiosResponse } from "axios";
import BaseService from 'mitsi_bot_lib/dist/src/BaseService';
import IntentDispatcherInput from 'mitsi_bot_lib/dist/src/IntentDispatcherInput';
import ResponseText from "mitsi_bot_lib/dist/src/ResponseText";
import LanguageProcessingApiInput from 'mitsi_bot_lib/dist/src/LanguageProcessingApiInput';


class Server extends BaseService {
  onPost(request: Request, response: Response): void {
    answer(request.body as LanguageProcessingApiInput)
      .then((result: ResponseText) => {
        response.json(result);
      }).catch((e: any) => {
        console.log("language-processing-api: process message, catch ", e);
        response.json(new ResponseText(e));
      });
  }
}

function answer(input: LanguageProcessingApiInput): Promise<ResponseText> {
  return new Promise(async (resolve, reject) => {
    const intent = await getIntent(config.intents, input.message);
    if (intent === undefined) {
      resolve(new ResponseText(`Wut ?`));
    } else if (intent === "GREETING") {
      resolve(new ResponseText(`Hey :)`));
    } else {
      const data = new IntentDispatcherInput(intent, !!input.isAuthenticated);
      axios
        .post(config.webservices.intentDispatcher, data)
        .then((response: AxiosResponse<ResponseText>) => {
          resolve(response.data);
        })
        .catch((err: any) => {
          console.error(err);
          reject(new ResponseText("An error has occured when contacting " + config.webservice.intentDispatcher));
        });
    }
  });
}

export function getIntent(services: any, message: string): any | undefined {
  return new Promise((resolve, _) => {
    Object.keys(services).forEach((key: string) => {
      if (services[key].includes(message)) {
        resolve(key);
        return;
      }
    });
    resolve(undefined);
  });
}

console.log("Config:", config.intents);
new Server(express()).startServer();