import { getIntent } from "../src";

describe("getIntent", () => {
  it("should return intent", () => {
    expect.assertions(1);
    const message = "!time";
    const conf = { TIME: ['!heure', '!hour', '!time'] };
    getIntent(conf, message).then((intent: string) => expect(intent).toBe("TIME"));
  });

  it("should return undefined", () => {
    expect.assertions(1);
    const message = "wtf";
    const conf = { TIME: ['!heure', '!hour', '!time'] };
    getIntent(conf, message).then((intent: string) => expect(intent).toBe(undefined));
  });
});
